//
//  Model.swift
//  Catie
//
//  Created by Thiago Vinhote on 15/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit

protocol ModelDelegate {
    func errorUpdating(error: NSError);
    func modelUpdated();
}

class Model {

    static let singleton = Model();
    
    private var container: CKContainer {
        return CKContainer.default();
    }
    
    public var publicDB : CKDatabase{
        return container.publicCloudDatabase;
    }
    
    public var privateDB: CKDatabase{
        return container.privateCloudDatabase;
    }
    
    private init() {
        
    }

}
