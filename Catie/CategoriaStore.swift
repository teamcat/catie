//
//  CategoriaStore.swift
//  Catie
//
//  Created by Thiago Vinhote on 15/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit
import CoreData

class CategoriaStore: NSObject {

    let model : Model = Model.singleton;
    var categorias = [Categoria]();
    var categoria : Categoria!
    var delegate : ModelDelegate?
    
    class Categoria {
        var record : CKRecord!;
        
        var nome: String {
            get{
                return self.record.object(forKey: "Nome") as! String;
            }
            set (val){
                record.setObject(val as CKRecordValue?, forKey: "Nome");
            }
        }
        
        init(dataBase: CKDatabase? = nil, record: CKRecord? = nil, recordName: String? = nil) {
            if let rName = recordName {
                self.record = (record != nil) ? record : CKRecord(recordType: "Categoria", recordID: CKRecordID(recordName: rName))
            }else{
                self.record = (record != nil) ? record : CKRecord(recordType: "Categoria");
            }
        }
        
        convenience init(nome: String) {
            self.init();
            
            self.nome = nome;
        }
    }
    
    init(categoria: Categoria? = nil) {
        super.init()
        
        self.categoria = categoria ?? Categoria();
    }
    
    
    
    func create(completion: @escaping (_ success: Bool, _ messagem: String, _ error: NSError?) -> Void){
        
        model.publicDB.save(categoria.record) { (record, error) in
            DispatchQueue.main.async {
                if error != nil {
                    completion(false, "Não foi possivel salvar no CloudKit", error as NSError?);
                }else{
                    self.categoria.record = record;
                    completion(true, "Salvo com sucesso", nil);
                }
            }
        }
    }
    
    func load(predicate: NSPredicate? = nil, sort: NSSortDescriptor? = nil, completion: ((_ sucesso: Bool, _ mensagem: String, _ error: NSError?) -> Void)?){
        let predicate = predicate ?? NSPredicate(value: true);
        let sort = sort ?? NSSortDescriptor(key: "Nome", ascending: false);
        let query = CKQuery(recordType: "Categoria", predicate: predicate);
        query.sortDescriptors = [sort];
        
        model.publicDB.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error as! NSError);
                    completion?(false, "Erro ao tentar carregar as categorias", error as NSError?)
                    print("Erro ao carregar: \((error as! NSError).description)");
                }
            }else{
                self.categorias.removeAll();
                
                for record in results! {
                    let categoria = Categoria(record: record);
                    self.categorias.append(categoria);
                }
                
                DispatchQueue.main.async {
                    self.delegate?.modelUpdated();
                    completion?(true, "Todas as categorias carregadas com sucesso.", nil)
                    print("Sucesso ao carregar.");
                }
            }
        }
    }
    
    func search(recordID: CKRecordID, completion: @escaping (_ success: Bool, _ categoria: Categoria?, _ error : NSError?) -> Void){
        let fetch = CKFetchRecordsOperation(recordIDs: [recordID]);
        
        fetch.perRecordProgressBlock = {
            print($1);
        }
        
        fetch.perRecordCompletionBlock = { record, recordId, error in
            if error != nil {
                DispatchQueue.main.async {
                    completion(false, nil, error as NSError?);
                    print(error.debugDescription);
                }
            }else{
                DispatchQueue.main.async {
                    completion(true, CategoriaStore.Categoria(record: record), nil);
                }
            }
        }
        
        fetch.database = model.publicDB;
        fetch.start();
    }
    
    func update(categoria: Categoria, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ) {
        self.categoria = categoria;
        
        create() { success, message, error in
            completion(success, message, error)
        }
    }
    
    func remove(recordId: CKRecordID, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ){
        model.publicDB.delete(withRecordID: recordId) { record, error in
            if error != nil {
                print("ERRO RECORD ID: \(self.categoria.record.recordID)")
                completion(false, "CloudKit não pode remover", error as NSError?)
            } else {
                print("DEL RECORD ID: \(self.categoria.record.recordID)")
                completion(true, "Deletado com sucesso", nil)
            }
        }
    }
    
}
