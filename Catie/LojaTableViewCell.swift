//
//  LojaTableViewCell.swift
//  Catie
//
//  Created by Arthur Melo on 9/15/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LojaTableViewCell: UITableViewCell {

    @IBOutlet var labelNome: UILabel!
    @IBOutlet var bannerImage: UIImageView!
    @IBOutlet var seguidoresLabel: UILabel!
    @IBOutlet var stars: CosmosView!
    @IBOutlet weak var stackServicos: UIStackView!
    
    var loja: LojaStore.Loja! {
        didSet {
            self.configure()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func configure() {
        self.labelNome.text = loja.nome
        self.bannerImage.image = ckAssetToUIImage(asset: self.loja.imagemCapa)
        self.stars.rating = Double(self.loja.avaliacao)
        let seguidores = self.loja.artesanal.hashValue * 100 + self.loja.cartao.hashValue * 65 + self.loja.entrega.hashValue * 140 + self.loja.encomenda.hashValue * 43
        self.seguidoresLabel.text = "\(seguidores) followers"
        
        let s = self.loja.artesanal.hashValue + self.loja.cartao.hashValue + self.loja.combinar.hashValue + self.loja.encomenda.hashValue + self.loja.encomenda.hashValue
        stars.rating = Double(s)
        
        self.stackServicos.subviews[0].isHidden = !self.loja.entrega
        self.stackServicos.subviews[1].isHidden = !self.loja.encomenda
        self.stackServicos.subviews[2].isHidden = !self.loja.combinar
        self.stackServicos.subviews[3].isHidden = !self.loja.artesanal
        self.stackServicos.subviews[4].isHidden = !self.loja.cartao
    }
}
