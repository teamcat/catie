//
//  LojaCD.swift
//  Catie
//
//  Created by Thiago Vinhote on 21/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import CoreData
import UIKit
import CloudKit

class LojaCD: NSManagedObject {

    @NSManaged var artesanal: NSNumber?
    @NSManaged var cartao: NSNumber?
    @NSManaged var entrega: NSNumber?
    @NSManaged var encomenda: NSNumber?
    @NSManaged var combinar: NSNumber?
    @NSManaged var nome: String?
    @NSManaged var email: String?
    @NSManaged var senha: String?
    @NSManaged var recordID: String?
    @NSManaged var recordReferenceCategoria: String?
    @NSManaged var imagemCapa: UIImage?
    @NSManaged var imagemPerfil: UIImage?
    
    func ckReference(recordName: String) -> CKReference{
        return CKReference(record: CKRecord(recordType: "Loja", recordID: CKRecordID(recordName: recordName)), action: .deleteSelf)
    }
    
    func convertLoja() -> LojaStore.Loja {
        let lojaCK = LojaStore.Loja(recordName: self.recordID)
        lojaCK.artesanal = self.artesanal! as Bool
        lojaCK.cartao = self.cartao! as Bool
        lojaCK.email = self.email!
        lojaCK.encomenda = self.encomenda! as Bool
        lojaCK.entrega = self.entrega! as Bool
        lojaCK.imagemCapa = uiImageToCKAsset(imagem: self.imagemCapa!)
        lojaCK.imagemPerfil = uiImageToCKAsset(imagem: self.imagemPerfil!)
        lojaCK.nome = self.nome!
        lojaCK.categoria = CategoriaStore.Categoria(recordName: self.recordReferenceCategoria)
        lojaCK.senha = self.senha!
        lojaCK.combinar = self.combinar! as Bool
        return lojaCK
    }
    
}
