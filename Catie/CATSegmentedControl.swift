//
//  CATSegmentedControl.swift
//  Catie
//
//  Created by Thiago Vinhote on 17/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

public struct CATSegmentedControlAppearance {
    var backgroundColor: UIColor
    var selectedBackgroundColor: UIColor
    var textColor: UIColor
    var selectedTextColor: UIColor
    var font: UIFont
    var bottomLineColor: UIColor
    var selectorColor: UIColor
    var bottomLineHeight: CGFloat
    var selectorHeight: CGFloat
    var labelTopPadding: CGFloat
    var bottomLine: Bool
}

@objc public protocol CATSegmentedControlDelegate {
    @objc optional func segmentedControlWillPressItemAtIndex(segmentedControl: CATSegmentedControl, index: Int)
    @objc optional func segmentedConrollDidPressedItemAtIndex(segmentedControl: CATSegmentedControl, index: Int)
}

typealias CATSegmentedControlAction = (_ segmentedControl: CATSegmentedControl, _ index: Int) -> Void

@IBDesignable
public class CATSegmentedControl: UIView {

    typealias CATSegmentedControlItemAction = (_ item: CATSegmentedControlItem) -> Void
    
    class CATSegmentedControlItem: UIControl {
        
        private var willPress: CATSegmentedControlItemAction?
        private var didPressed: CATSegmentedControlItemAction?
        var label: UILabel!
        
        init(frame: CGRect, text: String, appearance: CATSegmentedControlAppearance, willPress: CATSegmentedControlItemAction?, didPressed: CATSegmentedControlItemAction?){
            super.init(frame: frame)
            self.willPress = willPress
            self.didPressed = didPressed
            label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
            label.textColor = appearance.textColor
            label.font = appearance.font
            label.textAlignment = .center
            label.text = text
            self.addSubview(label)
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.willPress?(self)
        }
        
        override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.didPressed?(self)
        }
    }

    @IBInspectable
    var backgroundSelectedColor : UIColor = UIColor.gray {
        didSet{
            self.appearance.selectedBackgroundColor = backgroundSelectedColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var backgroundUnselectedColor : UIColor = UIColor.darkGray {
        didSet{
            self.appearance.backgroundColor = backgroundUnselectedColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var selectorColor : UIColor = UIColor.purple {
        didSet{
            self.appearance.selectorColor = selectorColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var selectorHeight : CGFloat = 3.0 {
        didSet{
            self.appearance.selectorHeight = selectorHeight
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var bottomLine : Bool = true {
        didSet{
            self.appearance.bottomLine = bottomLine
        }
    }
    
    @IBInspectable
    var bottomLineLineColor: UIColor = UIColor.white{
        didSet{
            self.appearance.bottomLineColor = bottomLineLineColor
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var bottomLineHeight : CGFloat = 3.0 {
        didSet{
            self.appearance.bottomLineHeight = bottomLineHeight
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var labelTopPadding : CGFloat = 1.0 {
        didSet{
            self.appearance.labelTopPadding = labelTopPadding
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var sizeFont: CGFloat = 18 {
        didSet{
            self.appearance.font = UIFont.systemFont(ofSize: sizeFont)
            self.setNeedsDisplay()
        }
    }
    
    weak var delegate: CATSegmentedControlDelegate?
    var action: CATSegmentedControlAction?
    
    var appearance : CATSegmentedControlAppearance! {
        didSet{
            self.draw()
            self.setNeedsDisplay()
        }
    }
    
    var titles: [String]! = ["Teste", "Teste"]
    
    var items: [CATSegmentedControlItem]!
    var selector: UIView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.defaultAppearance()
    }
    
    convenience init(frame: CGRect, titles: [String], action: CATSegmentedControlAction? = nil) {
        self.init(frame: frame)
        self.action = action
        self.titles = titles
        self.defaultAppearance()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.defaultAppearance()
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.draw()
    }
    
    private func defaultAppearance(){
        appearance = CATSegmentedControlAppearance(backgroundColor: UIColor.black, selectedBackgroundColor: UIColor.black, textColor: UIColor.white, selectedTextColor: UIColor.white, font: UIFont.systemFont(ofSize: 18), bottomLineColor: UIColor.darkGray, selectorColor: UIColor.cyan, bottomLineHeight: 3, selectorHeight: 3, labelTopPadding: 0, bottomLine: true)
    }
    
    private var width : CGFloat {
        return frame.size.width / CGFloat(titles.count)
    }
    
    private func draw(){
        self.reset()
        self.backgroundColor = appearance.backgroundColor
        
        var currentX: CGFloat = 0
        
        for title in self.titles {
            let item = CATSegmentedControlItem(frame: CGRect(x: currentX, y: appearance.labelTopPadding, width: width, height: frame.size.height - appearance.labelTopPadding), text: title, appearance: appearance, willPress: { (segmentedControlItem) in
                
                let index = self.items.index(of: segmentedControlItem)!
                self.delegate?.segmentedControlWillPressItemAtIndex?(segmentedControl: self, index: index)
                
                }, didPressed: { (segmentedControlItem) in
                    
                    let index = self.items.index(of: segmentedControlItem)!
                    self.selectItemAtIndex(index, true)
                    self.action?(self, index)
                    self.delegate?.segmentedConrollDidPressedItemAtIndex?(segmentedControl: self, index: index)
            })
            
            self.addSubview(item)
            items.append(item)
            currentX += width
        }
        
        if appearance.bottomLine {
            let bottomLine = CALayer()
            bottomLine.frame = CGRect(x: 0, y: frame.size.height - appearance.bottomLineHeight, width: frame.size.width, height: appearance.bottomLineHeight)
            bottomLine.backgroundColor = appearance.bottomLineColor.cgColor
            self.layer.addSublayer(bottomLine)
        }
        
        self.selector = UIView(frame: CGRect(x: 0, y: frame.size.height - appearance.selectorHeight, width: width, height: appearance.selectorHeight))
        self.selector.backgroundColor = appearance.selectorColor
        self.addSubview(selector)
        
        self.selectItemAtIndex(0, true)
    }
    
    private func selectItemAtIndex(_ index: Int, _ withAnimation: Bool){
        self.moveSelectorAtIndex(index: index, withAnimation: withAnimation)
        for item in self.items {
            if item == items[index]{
                item.label.textColor = appearance.selectedTextColor
                item.label.font = appearance.font
                item.backgroundColor = appearance.selectedBackgroundColor
            }else{
                item.label.textColor = appearance.textColor
                item.label.font = appearance.font
                item.backgroundColor = appearance.backgroundColor
            }
        }
    }
    
    private func moveSelectorAtIndex(index: Int, withAnimation: Bool){
        let w = self.width
        let target = w * CGFloat(index)
        
        
        
        UIView.animate(withDuration: withAnimation ? 0.3 : 0.0, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [], animations: {
            
            self.selector.frame.origin.x = target
            
            }, completion: nil)
    }
    
    private func reset(){
        for sub in self.subviews {
            sub.removeFromSuperview()
        }
        
        items = []
    }
    
}
