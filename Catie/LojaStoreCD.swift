//
//  LojaStoreCD.swift
//  Catie
//
//  Created by Thiago Vinhote on 21/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CoreData

class LojaStoreCD: NSObject {

    static let singleton = LojaStoreCD()
    
    private override init() {
        super.init()
    }
    
    private lazy var managedObjectContext : NSManagedObjectContext = {
        return (UIApplication.shared.delegate as! AppDelegate ).managedObjectContext
    }()
    
    func create() -> LojaCD {
        return NSEntityDescription.insertNewObject(forEntityName: "Loja", into: self.managedObjectContext) as! LojaCD
    }
    
    func save(loja: LojaCD){
        do{
            return try self.managedObjectContext.save()
        }catch{
            print("Error fetching records: \(error)")
        }
    }
    
    func delete(categoria: CategoriaCD){
        self.managedObjectContext.delete(categoria)
        do{
            try self.managedObjectContext.save()
        }catch{
            print("Error deleting record: \(error)")
        }
    }
    
    func get(completion: ((_ sucesso: Bool, _ categorias: [LojaCD]?, _ error: NSError?) -> Void)?) -> Void{
        let fetch = NSFetchRequest<LojaCD>(entityName: "Loja")
        fetch.sortDescriptors = [NSSortDescriptor(key: "nome", ascending: false)]
        
        do{
            let result = try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>) as! [LojaCD]
            
            if result.count == 0 {
                let categoriaStore = CategoriaStore()
                categoriaStore.load(completion: { (sucesso, mensagem, error) in
                    if sucesso {
                        print(mensagem)
                        
                        for categoria in categoriaStore.categorias {
                            let l = self.create()
                            l.nome = categoria.nome
                            l.recordID = categoria.record.recordID.recordName
                            print("Baixado", l.nome, l.recordID)
                            self.save(loja: l)
                        }
                        
                        print("Carregadas do CloudKit")
                        
                        do {
                            let r = try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>) as! [LojaCD]
                            completion?(true, r, nil)
                        }catch {
                            completion?(false, nil, error as NSError?)
                        }
                    }else{
                        print(mensagem, error?.description)
                        completion?(false, nil, error as NSError?)
                    }
                })
            }else{
                print("Carregadas do CoreData")
                completion?(true, result, nil)
            }
            
        }catch{
            print("Error fetching records: \(error)")
            completion?(false, nil, error as NSError?)
        }
    }
    
    func getFilterBy(recordName: String? = nil) -> LojaCD? {
        let fetch = NSFetchRequest<LojaCD>(entityName: "Loja")
        if let rc = recordName {
            fetch.predicate = NSPredicate(format: "recordID == %@", rc)
        }
        
        do{
            return try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>).first as? LojaCD
        }catch{
            print("Error fetching records: \(error)")
            return nil
        }
    }
    
}
