//
//  LojaProdutoTableViewCell.swift
//  Catie
//
//  Created by Thiago Vinhote on 26/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LojaProdutoTableViewCell: UITableViewCell {

    @IBOutlet weak var stackProduto: UIStackView!
    @IBOutlet weak var viewProduto1: UIView!
    @IBOutlet weak var viewProduto2: UIView!
    @IBOutlet weak var labelPreco1: UILabel!
    @IBOutlet weak var labelPreco2: UILabel!
    
    var produtos: (produto1: ProdutoStore.Produto?, produto2: ProdutoStore.Produto?)! {
        didSet{
            self.configure()
        }
    }
    
    private func configure(){
        let imagem1 = viewProduto1.viewWithTag(1) as! UIImageView
        imagem1.image = ckAssetToUIImage(asset: self.produtos.produto1?.imagens[0])
        
        let imagem2 = viewProduto2.viewWithTag(1) as! UIImageView
        imagem2.image = ckAssetToUIImage(asset: self.produtos.produto2?.imagens[0])
        
        let label1 = viewProduto1.viewWithTag(2) as! UILabel
        label1.text = self.produtos.produto1?.nome
        
        let label2 = viewProduto2.viewWithTag(2) as! UILabel
        label2.text = self.produtos.produto2?.nome
        
        labelPreco1.text = "\(self.produtos.produto1!.valor)"
        labelPreco2.text = "\(self.produtos.produto2!.valor)"
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
