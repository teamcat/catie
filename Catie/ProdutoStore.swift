//
//  ProdutoStore.swift
//  Catie
//
//  Created by Thiago Vinhote on 15/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit
import UIKit

class ProdutoStore: NSObject {

    let model : Model = Model.singleton;
    var produtos = [Produto]();
    var produto : Produto!;
    var delegate : ModelDelegate?
    
    class Produto {
        var record: CKRecord!;
        
        
        var nome: String {
            get {
                return self.record.object(forKey: "Nome") as! String;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Nome");
            }
        }
        
        var valor: Double {
            get {
                return self.record.object(forKey: "Valor") as! Double
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Valor");
            }
        }
        
        var data: NSDate {
            get{
                return self.record.object(forKey: "Data") as! NSDate;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Data");
            }
        }
        
        var imagens: [CKAsset]{
            get {
                return self.record.object(forKey: "Imagens") as! [CKAsset];
            }
            set (val){
                self.record.setValue(val as CKRecordValue?, forKey: "Imagens");
            }
        }
        
        var descricao: String{
            get {
                return self.record.object(forKey: "Descricao") as! String;
            }
            set (val){
                self.record.setValue(val as CKRecordValue?, forKey: "Descricao")
            }
        }
        
        var loja: LojaStore.Loja {
            get {
                let reference = self.record.object(forKey: "Loja") as! CKReference
                return LojaStore.Loja(recordName: reference.recordID.recordName)
            }
            set (val){
                let reference = CKReference(recordID: val.record.recordID, action: .deleteSelf);
                self.record.setObject(reference as CKRecordValue?, forKey: "Loja");
            }
        }
        
        var reference: CKReference {
            get {
                return CKReference(record: self.record, action: .deleteSelf)
            }
        }
        
        var referenceLoja : CKReference{
            get {
                return self.record.object(forKey: "Loja") as! CKReference
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Loja")
            }
        }
        
        var referenceCategoria: CKReference {
            get {
                return self.record.object(forKey: "Categoria") as! CKReference
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Categoria")
            }
        }
        
        var categoria: CategoriaStore.Categoria {
            get {
                let reference = self.record.object(forKey: "Categoria") as! CKReference
                return CategoriaStore.Categoria(recordName: reference.recordID.recordName);
            }
            set (val){
                let reference = CKReference(recordID: val.record.recordID, action: .deleteSelf);
                self.record.setObject(reference as CKRecordValue?, forKey: "Categoria");
                
            }
        }
        
        init(dataBase: CKDatabase? = nil, record: CKRecord? = nil) {
            self.record = (record != nil) ? record : CKRecord(recordType: "Produto");
        }
        
        convenience init(nome: String, valor: Double, data: NSDate, imagens: [CKAsset], descricao: String){
            self.init();
            
            self.nome = nome;
            self.valor = valor;
            self.data = data;
            self.imagens = imagens;
            self.descricao = descricao;
        }
    }
    init(produto: Produto? = nil) {
        super.init()
        
        self.produto = produto ?? Produto();
    }
    
    func create(completion: @escaping (_ success: Bool, _ messagem: String, _ error: NSError?) -> Void){
        
        model.publicDB.save(produto.record) { (record, error) in
            DispatchQueue.main.async {
                if error != nil {
                    completion(false, "Something wrong happened.\nIt wasn't possible to register the product.\nCheck your internet connection.", error as NSError?);
                }else{
                    self.produto.record = record;
                    completion(true, "Product posted to your store", nil);
                }
            }
        }
    }
    
    func load(predicate: NSPredicate? = nil, sort: NSSortDescriptor? = nil){
        
//        self.produtos = produtosStore
//        self.delegate?.modelUpdated();
//        return
        
        let predicate = predicate ?? NSPredicate(value: true);
        let sort = sort ?? NSSortDescriptor(key: "Nome", ascending: false);
        let query = CKQuery(recordType: "Produto", predicate: predicate);
        query.sortDescriptors = [sort];
        
        model.publicDB.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error as! NSError);
                    print("Erro ao carregar: \((error as! NSError).description)");
                    return;
                }
            }else{
                self.produtos.removeAll();
                
                for record in results! {
                    let produto = Produto(record: record);
                    self.produtos.append(produto);
                }
                
                DispatchQueue.main.async {
                    self.delegate?.modelUpdated();
                    print("Sucesso ao carregar.");
                    return;
                }
            }
        }
    }
    
    func search(recordIDs: [CKRecordID], completion: @escaping (_ success : Bool, _ produtos: Produto?, _ error : NSError?) -> Void){
        
        let fetch = CKFetchRecordsOperation(recordIDs: recordIDs);
        
        fetch.perRecordProgressBlock = {
            print($1);
        }
        
        fetch.perRecordCompletionBlock = { record, recordId, error in
            if error != nil {
                completion(false, nil, error as NSError?);
                print(error.debugDescription);
            }else{
                DispatchQueue.main.async {
                    completion(true, Produto(record: record), nil);
                }
            }
        }
        
        fetch.database = model.publicDB;
        fetch.start();
    }
    
    func update(produto: Produto, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ) {
        self.produto = produto;
        
        create() { success, message, error in
            completion(success, message, error)
        }
    }
    
    func remove(recordId: CKRecordID, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ){
        model.publicDB.delete(withRecordID: recordId) { record, error in
            if error != nil {
                print("ERRO RECORD ID: \(self.produto.record.recordID)")
                completion(false, "CloudKit não pode remover", error as NSError?)
            } else {
                print("DEL RECORD ID: \(self.produto.record.recordID)")
                completion(true, "Deletado com sucesso", nil)
            }
        }
    }
    
    private var produtosStore : [Produto] = {
        
        var produtos = [Produto]()
        
        for i in 0...6 {
            let produto = Produto()
            produto.nome = "Produto \(i)"
            produto.data = NSDate()
            produto.descricao = "Descrição do produto \(i)"
            produto.valor = 30.6 * Double(i)
            produto.categoria = CategoriaStore.Categoria(recordName: (i % 2 == 0) ? "f73877ab-849b-438c-b52d-23578156ee52" : "A78FD2FE-3890-4AC9-817F-FA770894AC7E")
            if i % 2 == 0 {
                produto.imagens = [uiImageToCKAsset(imagem: UIImage(named: "canecas")!)]
            }else{
                produto.imagens = [uiImageToCKAsset(imagem: UIImage(named: "quadros")!)]
            }
            produtos.append(produto)
        }
        
        return produtos
    }()
}
