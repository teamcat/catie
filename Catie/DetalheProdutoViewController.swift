//
//  DetalhelProdutoViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CloudKit

class DetalheProdutoViewController: UIViewController {

    private let produtoStore = ProdutoStore();
    internal  var categorias: [CategoriaCD]!
    internal weak var categoriaSelecionada : CategoriaCD!
    
    @IBOutlet weak var imagemProduto: UIImageView!
    @IBOutlet weak var nomeProduto: UITextField!
    @IBOutlet weak var descricaoProduto: UITextView!
    @IBOutlet weak var valorProduto: UITextField!
    @IBOutlet weak var categoriaProduto: UITextField!
    @IBOutlet weak var scrollImagens: UIScrollView!
    
    internal var imagensProdutos : [UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CategoriaStoreCD.singleton.get(){ sucesso, categorias, error in
            if sucesso {
                self.categorias = categorias!
                
                self.categoriaProduto.keyboardPicker(categorias!.map({ (categoria) -> String in
                    return categoria.nome!
                }) , controller: self)
                
            }else{
                print(error?.description)
            }
        }
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapView)))
        self.imagensProdutos = []
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc internal func tapView(){
        self.view.endEditing(true)
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        produtoStore.produto = ProdutoStore.Produto()
        produtoStore.produto.nome = self.nomeProduto.text!
        produtoStore.produto.valor = Double(self.valorProduto.text!)!
        produtoStore.produto.data = NSDate()
        produtoStore.produto.descricao = self.descricaoProduto.text!

        produtoStore.produto.imagens = self.imagensProdutos.map({ (imagem) -> CKAsset in
            return uiImageToCKAsset(imagem: imagem)
        })
        
        produtoStore.produto.categoria = CategoriaStore.Categoria(recordName: categoriaSelecionada.recordID!)
        let loja = LojaStoreCD.singleton.getFilterBy()
        if let recordName = loja?.recordID {
            produtoStore.produto.loja = LojaStore.Loja(recordName: recordName)
        }
        
        self.lock()
        sender.isEnabled = false
        produtoStore.create { (sucesso, mensagem, error) in
            var alertControl: UIAlertController!
            if sucesso {
                print(mensagem);
                alertControl = UIAlertController(title: "Product", message: mensagem, preferredStyle: .alert)
                let action = UIAlertAction(title: "Back", style: .default, handler: { (action) in
                    _ = self.navigationController!.popViewController(animated: true);
                })
                alertControl.addAction(action)
            }else{
                print(mensagem, error?.description);
                alertControl = UIAlertController(title: "Product", message: mensagem, preferredStyle: .alert)
                let action = UIAlertAction(title: "Close", style: .default, handler: { action in
                    sender.isEnabled = true
                })
                alertControl.addAction(action)
            }
            
            self.present(alertControl, animated: true, completion: nil);
            self.unlock()
        }
        
    }
    
    @IBAction func buttonImage(_ sender: UIButton){
        
        let menuController = UIAlertController(title: "Foto", message: "Local da foto", preferredStyle: .actionSheet)
        menuController.addAction(UIAlertAction(title: "Câmera", style: .default, handler: {action in
        
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }))
        menuController.addAction(UIAlertAction(title: "Galeria", style: .default, handler: { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        }))
        menuController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(menuController, animated: true, completion: nil)
    }
}


extension DetalheProdutoViewController : UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.imagensProdutos.append(info[UIImagePickerControllerOriginalImage] as! UIImage)
        imagemProduto.contentMode = .scaleToFill
        self.imagemProduto.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetalheProdutoViewController : UINavigationControllerDelegate{
    
}

extension DetalheProdutoViewController: UIPickerViewDelegate{
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.categoriaSelecionada = self.categorias[row]
        self.categoriaProduto.text = self.categoriaSelecionada.nome!
    }
    
}

extension DetalheProdutoViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let distanciaVertical = self.view.frame.height - textField.frame.origin.y - textField.frame.size.height / 2
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform(translationX: 0, y: (distanciaVertical - 300) < 0 ? (distanciaVertical - 300) : 0)
            
            }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform.identity
            
            }, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tapView()
        return true
    }
    
}

extension DetalheProdutoViewController: UITextViewDelegate {
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "Description..." {
            textView.text = ""
        }
        
        let distanciaVertical = self.view.frame.height - textView.frame.origin.y - textView.frame.size.height / 2
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform(translationX: 0, y: (distanciaVertical - 250) < 0 ? (distanciaVertical - 250) : 0)
            
            }, completion: nil)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            textView.text = "Description..."
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform.identity
            
            }, completion: nil)
    }
    
}

extension DetalheProdutoViewController : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categorias.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categorias[row].nome!
    }
}
