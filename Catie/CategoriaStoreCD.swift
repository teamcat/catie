//
//  CategoriaStoreCD.swift
//  Catie
//
//  Created by Thiago Vinhote on 17/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CoreData

class CategoriaStoreCD: NSObject {

    static let singleton = CategoriaStoreCD()
    
    private override init() {
        super.init()
    }
    
    private lazy var managedObjectContext : NSManagedObjectContext = {
        return (UIApplication.shared.delegate as! AppDelegate ).managedObjectContext
    }()
    
    func create() -> CategoriaCD{
        return NSEntityDescription.insertNewObject(forEntityName: "Categoria", into: self.managedObjectContext) as! CategoriaCD
    }
    
    func save(categoria: CategoriaCD){
        do{
            return try self.managedObjectContext.save()
        }catch{
            print("Error fetching records: \(error)")
        }
    }
    
    func delete(categoria: CategoriaCD){
        self.managedObjectContext.delete(categoria)
        do{
            try self.managedObjectContext.save()
        }catch{
            print("Error deleting record: \(error)")
        }
    }
    
    func get(completion: ((_ sucesso: Bool, _ categorias: [CategoriaCD]?, _ error: NSError?) -> Void)?) -> Void{
        let fetch = NSFetchRequest<CategoriaCD>(entityName: "Categoria")
        fetch.sortDescriptors = [NSSortDescriptor(key: "nome", ascending: false)]
        
        do{
            let result = try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>) as! [CategoriaCD]
            
            if result.count == 0 {
                let categoriaStore = CategoriaStore()
                categoriaStore.load(completion: { (sucesso, mensagem, error) in
                    if sucesso {
                        print(mensagem)
                        
                        for categoria in categoriaStore.categorias {
                            let c = self.create()
                            c.nome = categoria.nome
                            c.recordID = categoria.record.recordID.recordName
                            print("Baixado", c.nome, c.recordID)
                            self.save(categoria: c)
                        }
                        
                        print("Carregadas do CloudKit")
                        
                        do {
                            let r = try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>) as! [CategoriaCD]
                            completion?(true, r, nil)
                        }catch {
                            completion?(false, nil, error as NSError?)
                        }
                    }else{
                        print(mensagem, error?.description)
                        completion?(false, nil, error as NSError?)
                    }
                })
            }else{
                print("Carregadas do CoreData")
                completion?(true, result, nil)
            }
            
        }catch{
            print("Error fetching records: \(error)")
            completion?(false, nil, error as NSError?)
        }
    }
    
    func getFilterBy(recordName: String? = nil) -> CategoriaCD? {
        let fetch = NSFetchRequest<CategoriaCD>(entityName: "Categoria")
        if let rc = recordName {
            fetch.predicate = NSPredicate(format: "recordID == %@", rc)
        }
        
        do{
            return try self.managedObjectContext.fetch(fetch as! NSFetchRequest<NSFetchRequestResult>).first as? CategoriaCD
        }catch{
            print("Error fetching records: \(error)")
            return nil
        }
    }
    
}
