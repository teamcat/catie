//
//  ViewController.swift
//  Catie
//
//  Created by Arthur Melo on 9/13/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var segmentedControl: CATSegmentedControl!
    
    var lojasViewController: LojaTableViewController!
    var produtosViewController: ProdutoCollectionViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let sb = storyboard {
            lojasViewController = sb.instantiateViewController(withIdentifier: "lojasTableView") as! LojaTableViewController
            
            switchViewController(from: nil, to: lojasViewController)
        } else {
            print("storyboard is nil")
        }
        
        self.segmentedControl.titles = ["Store", "Products"]
        self.segmentedControl.delegate = self
    }

    func switchViewController(from fromVC: UIViewController?, to toVC: UIViewController?) {
        if let from = fromVC {
            from.willMove(toParentViewController: nil)
            from.view.removeFromSuperview()
            from.removeFromParentViewController()
        } else {
            print("fromVC is nil")
        }
        
        if let to = toVC {
            self.addChildViewController(to)
            to.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.width, height: containerView.frame.height);
            self.containerView.insertSubview(to.view, at: 0)
            to.didMove(toParentViewController: self)
        } else {
            print("toVC is nil")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeViewController() {
        if let lojaVC = lojasViewController {
            if let _ = lojaVC.parent {
                print("lojaVC is using")
            } else {
                print("set lojaVC = nil")
                lojasViewController = nil
            }
        }
        
        if let produtoVC = produtosViewController {
            if let _ = produtoVC.parent {
                print("produtoVC is using")
            } else {
                print("set produtoVC = nil")
                produtosViewController = nil
            }
        }
    }
}

extension HomeViewController : CATSegmentedControlDelegate{
    
    func segmentedConrollDidPressedItemAtIndex(segmentedControl: CATSegmentedControl, index: Int) {
        print(segmentedControl.titles[index])
        
        switch index {
            case 0:
                if let lojaVC = self.lojasViewController {
                    self.switchViewController(from: self.produtosViewController, to: lojaVC)
                } else {
                    if let sb = self.storyboard {
                        self.lojasViewController = sb.instantiateViewController(withIdentifier: "lojasTableView") as! LojaTableViewController
                        self.switchViewController(from: self.produtosViewController, to: self.lojasViewController)
                    } else {
                        print("storyboard is nil")
                    }
                }
            default:
                if let produtoVC = self.produtosViewController {
                    self.switchViewController(from: self.lojasViewController, to: produtoVC)
                } else {
                    if let sb = self.storyboard {
                        self.produtosViewController = sb.instantiateViewController(withIdentifier: "produtosCollectionView") as! ProdutoCollectionViewController
                        self.switchViewController(from: self.lojasViewController, to: self.produtosViewController)
                    } else {
                        print("storyboard is nil")
                    }
                }
            }
    }
    
}
