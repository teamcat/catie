//
//  PerfilViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class PerfilLojaTableViewController: UITableViewController {

    var loja: LojaStore.Loja!
    var produtoStore : ProdutoStore = ProdutoStore()
    var lojaStore = LojaStore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loja = LojaStoreCD.singleton.getFilterBy()?.convertLoja()
        produtoStore.delegate = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor.darkGray
        self.refreshControl?.tintColor = UIColor.white
        self.refreshControl?.addTarget(self, action: #selector(self.carregarDados), for: .valueChanged)
        
        lojaStore.search(recordID: self.loja.record.recordID) { (sucesso, loja, error) in
            if sucesso {
                self.loja = loja
                self.title = self.loja.nome
                self.carregarDados()
            } else {
                print(error?.description)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lock()
        carregarDados()
    }
    
    @objc private func carregarDados() -> Void {
        produtoStore.load(predicate: NSPredicate(format: "Loja = %@", loja!.reference))
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if loja != nil {
            return 1
        }
        return 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.produtoStore.produtos.count) + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLoja") as! LojaDetalheTableViewCell
            
            cell.loja = loja!
        
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLojaProduto") as! LojaProdutoTableViewCell
            
            cell.produtos = (produtoStore.produtos[indexPath.row - 1], produtoStore.produtos[indexPath.row - 1])
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 317
        }else{
            return 240
        }
    }

}

extension PerfilLojaTableViewController: ModelDelegate {
    
    func modelUpdated() {
        self.tableView.reloadData()
        self.unlock()
        if (self.refreshControl != nil) {
            let formate = DateFormatter()
            formate.dateFormat = "MMM d, h:mm a"
            let title = "Last update: \(formate.string(from: Date()))"
            let dictionary = [NSForegroundColorAttributeName: UIColor.white]
            let attribute = NSAttributedString(string: title, attributes: dictionary)
            self.refreshControl?.attributedTitle = attribute
            self.refreshControl?.endRefreshing()
        }
    }
    
    func errorUpdating(error: NSError) {
        if self.refreshControl != nil {
            self.refreshControl?.endRefreshing()
        }
        self.unlock()
        let alert = UIAlertController(title: "Stores", message: "There was a problem loading stores\nCheck your internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
