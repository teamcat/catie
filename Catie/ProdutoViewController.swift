//
//  ProdutoViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 23/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class ProdutoViewController: UIViewController {

    internal weak var produto : ProdutoStore.Produto!
    @IBOutlet weak var labelNomeProduto: UILabel!
    @IBOutlet weak var labelValorProduto: UILabel!
    @IBOutlet weak var labelDescricaoProduto: UILabel!
    @IBOutlet weak var labelCategoriaProduto: UILabel!
    @IBOutlet weak var imagemProduto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setProduto()
    }
    
    private func setProduto(){
        self.imagemProduto.layer.borderColor = UIColor.white.cgColor
        self.imagemProduto.layer.borderWidth = 2.0
        self.labelNomeProduto.text = self.produto.nome
        self.labelValorProduto.text = String(self.produto.valor)
        self.labelDescricaoProduto.text = self.produto.descricao
        self.imagemProduto.image = ckAssetToUIImage(asset: self.produto.imagens[0])
        self.labelCategoriaProduto.text = CategoriaStoreCD.singleton.getFilterBy(recordName: self.produto.categoria.record.recordID.recordName)?.nome!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "irParaLoja" {
            let destino = segue.destination as! LojaDetalheViewController
            destino.loja = self.produto.loja
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
