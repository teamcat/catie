//
//  ProdutoCollectionViewCell.swift
//  Catie
//
//  Created by Arthur Melo on 9/16/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class ProdutoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imagemProduto: UIImageView!
    @IBOutlet var labelNome: UILabel!
    @IBOutlet var labelValor: UILabel!
    @IBOutlet weak var labelNomeLoja: UILabel!
    
    static let identifier = "CarouselCollectionViewCell"
    
    var produto: ProdutoStore.Produto! {
        didSet {
            self.configure()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func configure() {
        self.labelNome.text = produto.nome
        self.imagemProduto.image = ckAssetToUIImage(asset: self.produto.imagens[0])
        self.labelValor.text = String(produto.valor)
    }
}
