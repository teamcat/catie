//
//  Util.swift
//  Catie
//
//  Created by Thiago Vinhote on 16/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CloudKit

public func ckAssetToUIImage(asset: CKAsset? = nil) -> UIImage?{
    
    if asset == nil {
        return UIImage()
    }
    
    if let data = NSData(contentsOf: asset!.fileURL) {
        let imagem = UIImage(data: data as Data);
        
        return imagem;
    }
    
    return nil;
}

public func uiImageToCKAsset(imagem: UIImage) -> CKAsset{
    var imageURL: NSURL!
    let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    let tempImageName = "\(NSUUID().uuidString)temp_image.jpg"
    
    let imageData: NSData = UIImageJPEGRepresentation(imagem, 0.8)! as NSData
    let path = documentsDirectoryPath.appendingPathComponent(tempImageName)
    imageURL = NSURL(fileURLWithPath: path)
    imageData.write(to: imageURL as URL, atomically: true)
    
    if let url = imageURL {
        return CKAsset(fileURL: url as URL)
    }
    
    let fileURL = Bundle.main.url(forResource: "noImage", withExtension: "jpg")
    return CKAsset(fileURL: fileURL!)
}


public func uiImageToCKAsset(named: String) -> CKAsset{
    var imageURL: NSURL!
    let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    let tempImageName = "temp_image.jpg"
    
    let imageData: NSData = UIImageJPEGRepresentation(UIImage(named: named)!, 0.8)! as NSData
    let path = documentsDirectoryPath.appendingPathComponent(tempImageName)
    imageURL = NSURL(fileURLWithPath: path)
    imageData.write(to: imageURL as URL, atomically: true)
    
    if let url = imageURL {
        return CKAsset(fileURL: url as URL)
    }
    
    let fileURL = Bundle.main.url(forResource: "noImage", withExtension: "jpg")
    return CKAsset(fileURL: fileURL!)
}
