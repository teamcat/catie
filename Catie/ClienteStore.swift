//
//  ClienteStore.swift
//  Catie
//
//  Created by Thiago Vinhote on 15/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit

class ClienteStore: NSObject {

    let model : Model = Model.singleton;
    var clientes = [Cliente]();
    var cliente : Cliente!;
    var delegate : ModelDelegate?
    
    class Cliente {
        var record : CKRecord!;
        
        var nome: String{
            get {
                return self.record.object(forKey: "Nome") as! String
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Nome");
            }
        }
        
        var dataNascimento: NSDate {
            get {
                return self.record.object(forKey: "DataNascimento") as! NSDate;
            }
            set (val) {
                self.record.setObject(val as CKRecordValue?, forKey: "DataNascimento");
            }
        }
        
        var imagemPerfil : CKAsset{
            get {
                return self.record.object(forKey: "ImagemPerfil") as! CKAsset;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "ImagemPerfil");
            }
        }
        
        var sexo : String{
            get {
                return self.record.object(forKey: "Sexo") as! String;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Sexo");
            }
        }
        
        init(dataBase: CKDatabase? = nil, record: CKRecord? = nil){
            self.record = (record != nil) ? record : CKRecord(recordType: "Cliente");
        }
        
        convenience init(nome: String, dataNascimento: NSDate, imagemPerfil: CKAsset, sexo: String  ){
            self.init();
            
            self.nome = nome;
            self.dataNascimento = dataNascimento;
            self.imagemPerfil = imagemPerfil;
            self.sexo = sexo;
        }
    }
    
    init(cliente: Cliente? = nil) {
        super.init();
        
        self.cliente = cliente ?? Cliente();
    }
    
    func create(completion: @escaping (_ success: Bool, _ messagem: String, _ error: NSError?) -> Void){
        
        model.publicDB.save(cliente.record) { (record, error) in
            DispatchQueue.main.async {
                if error != nil {
                    completion(false, "Não foi possivel salvar no CloudKit", error as NSError?);
                }else{
                    self.cliente.record = record;
                    completion(true, "Salvo com sucesso", nil);
                }
            }
        }
    }
    
    func load(predicate: NSPredicate? = nil, sort: NSSortDescriptor? = nil){
        let predicate = predicate ?? NSPredicate(value: true);
        let sort = sort ?? NSSortDescriptor(key: "Nome", ascending: false);
        let query = CKQuery(recordType: "Cliente", predicate: predicate);
        query.sortDescriptors = [sort];
        
        model.publicDB.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error as! NSError);
                    print("Erro ao carregar: \((error as! NSError).description)");
                    return;
                }
            }else{
                self.clientes.removeAll();
                
                for record in results! {
                    let cliente = Cliente(record: record);
                    self.clientes.append(cliente);
                }
                
                DispatchQueue.main.async {
                    self.delegate?.modelUpdated();
                    print("Sucesso ao carregar.");
                    return;
                }
            }
        }
    }
    
    func update(cliente: Cliente, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ) {
        self.cliente = cliente;
        
        create() { success, message, error in
            completion(success, message, error)
        }
    }
    
    func remove(recordId: CKRecordID, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ){
        model.publicDB.delete(withRecordID: recordId) { record, error in
            if error != nil {
                print("ERRO RECORD ID: \(self.cliente.record.recordID)")
                completion(false, "CloudKit não pode remover", error as NSError?)
            } else {
                print("DEL RECORD ID: \(self.cliente.record.recordID)")
                completion(true, "Deletado com sucesso", nil)
            }
        }
    }
    
}
