//
//  Categoria.swift
//  Catie
//
//  Created by Thiago Vinhote on 17/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CoreData

class CategoriaCD: NSManagedObject {

    @NSManaged var nome: String?
    @NSManaged var recordID: String?
    
    override var description: String {
        get{
            return "\(nome) \(recordID)"
        }
    }

}
