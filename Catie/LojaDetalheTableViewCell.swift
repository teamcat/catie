//
//  LojaDetalheTableViewCell.swift
//  Catie
//
//  Created by Thiago Vinhote on 26/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LojaDetalheTableViewCell: UITableViewCell {

    @IBOutlet weak var imagemCapa: UIImageView!
    @IBOutlet weak var imagemPerfil: UIImageView!
    @IBOutlet weak var labelNumeroSeguidores: UILabel!
    @IBOutlet weak var labelNomeCategoria: UILabel!
    
    @IBOutlet weak var iconEncomenda: UIImageView!
    @IBOutlet weak var iconEntrega: UIImageView!
    @IBOutlet weak var iconCartao: UIImageView!
    @IBOutlet weak var iconArtesanal: UIImageView!
    @IBOutlet weak var iconCombinar: UIImageView!
    
    weak var loja: LojaStore.Loja! {
        didSet{
            self.configure()
        }
    }
    
    private func configure(){
        self.imagemCapa.image = ckAssetToUIImage(asset: self.loja.imagemCapa)
        self.imagemPerfil.image = ckAssetToUIImage(asset: self.loja.imagemPerfil)
        let seguidores = self.loja.artesanal.hashValue * 100 + self.loja.cartao.hashValue * 65 + self.loja.entrega.hashValue * 140 + self.loja.encomenda.hashValue * 43
        self.labelNumeroSeguidores.text = String(seguidores)
        self.labelNomeCategoria.text = CategoriaStoreCD.singleton.getFilterBy(recordName: self.loja.categoria.record.recordID.recordName)?.nome!
        
        let layer = self.imagemPerfil.layer
        layer.borderWidth = 20
        layer.borderColor = UIColor.black.withAlphaComponent(0.70).cgColor
        layer.cornerRadius = CGFloat.maximum(self.imagemPerfil.frame.width, self.imagemPerfil.frame.height) / 2
        
        self.iconEncomenda.image = self.loja.encomenda ? #imageLiteral(resourceName: "encomenda-on-detalhe") : #imageLiteral(resourceName: "encomenda-off-detalhe")
        self.iconEntrega.image = self.loja.entrega ? #imageLiteral(resourceName: "entrega-on-detalhe") : #imageLiteral(resourceName: "entrega-off-detalhe")
        self.iconCartao.image = self.loja.cartao ? #imageLiteral(resourceName: "cartao-on-detalhe") : #imageLiteral(resourceName: "cartao-off-detalhe")
        self.iconArtesanal.image = self.loja.artesanal ? #imageLiteral(resourceName: "artesanal-on-detalhe") : #imageLiteral(resourceName: "artesanal-off-detalhe")
        self.iconCombinar.image = self.loja.combinar ? #imageLiteral(resourceName: "combinar-on-detalhe") : #imageLiteral(resourceName: "combinar-off-detalhe")
        
        let transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        self.iconEncomenda.transform = transform
        self.iconEntrega.transform = transform
        self.iconCartao.transform = transform
        self.iconArtesanal.transform = transform
        self.iconCombinar.transform = transform
        
        UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            
            self.iconEncomenda.transform = CGAffineTransform.identity
            
            }, completion: {f in
        
                UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                    
                    self.iconEntrega.transform = CGAffineTransform.identity
                    
                    }, completion: {f in
                
                        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                            
                            self.iconCartao.transform = CGAffineTransform.identity
                            
                            }, completion: {f in
                        
                                UIView.animate(withDuration: 0.2, delay: 0.0, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                                    
                                    self.iconArtesanal.transform = CGAffineTransform.identity
                                    
                                    }, completion: { f in
                                
                                
                                        UIView.animate(withDuration: 0.15, delay: 0.0, usingSpringWithDamping: 0.35, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                                            
                                            self.iconCombinar.transform = CGAffineTransform.identity
                                            
                                            }, completion: nil)
                                })
                                
                        })
                        
                })
                
        })
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
