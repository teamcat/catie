//
//  ProdutoCollectionViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

private enum CelulaColletionView : String {
    case cell = "Cell"
    case detalhe = "CellDetalhe"
}

class ProdutoCollectionViewController: UICollectionViewController {

    let produtoStore = ProdutoStore()
    var refreshControl: UIRefreshControl!
    
    var currentPage : Int = 0 {
        didSet{

        }
    }
    
    var pageSize: CGSize {
        guard let layout : CATCarouselFlowLayout = self.collectionView?.collectionViewLayout as? CATCarouselFlowLayout else {
            fatalError("Erro ao tentar pegar o layout da collection")
        }
        var pageSize = layout.itemSize
        if layout.scrollDirection == .horizontal {
            pageSize.width += layout.minimumLineSpacing
        }else{
            pageSize.height += layout.minimumLineSpacing
        }
        return pageSize
    }
    
    var orientation: UIDeviceOrientation {
        return UIDevice.current.orientation
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        produtoStore.delegate = self
    
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor.darkGray
        self.refreshControl?.tintColor = UIColor.white
        self.refreshControl?.addTarget(self, action: #selector(self.carregarDados), for: .valueChanged)
        collectionView?.addSubview(refreshControl)
        collectionView?.alwaysBounceVertical = true
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handlePress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.5
        self.collectionView?.addGestureRecognizer(lpgr)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupLayout(){
        let layout = self.collectionView?.collectionViewLayout as? CATCarouselFlowLayout
        layout?.spacingMode = .overlap(visibleOffset: 5)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lock()
        self.carregarDados()
    }
    
    @objc private func carregarDados(){
        produtoStore.load()
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "produtoView" {
            let destino = segue.destination as! ProdutoViewController
            destino.produto = self.produtoStore.produtos[(self.collectionView?.indexPathsForSelectedItems![0].row)!]
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.produtoStore.produtos.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.collectionViewLayout is CATCarouselFlowLayout {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CelulaColletionView.detalhe.rawValue, for: indexPath) as! ProdutoCollectionViewCell
            cell.produto = self.produtoStore.produtos[indexPath.row]
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CelulaColletionView.cell.rawValue, for: indexPath) as! ProdutoCollectionViewCell
        cell.produto = self.produtoStore.produtos[indexPath.row]
        return cell
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if self.collectionView?.collectionViewLayout is CATCarouselFlowLayout {
            print(#function, "CatCarouselFlowLayout")
            let layout = self.collectionView?.collectionViewLayout as? CATCarouselFlowLayout
            let pageSide = (layout?.scrollDirection == .horizontal) ? self.pageSize.width : self.pageSize.height
            let offset = (layout?.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
            currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
        }else{
            print(#function, "UICollectionViewFlowLayout")
        }
    }
    
    func handlePress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizerState.ended {
            return
        }
        if self.collectionView?.collectionViewLayout is CATCarouselFlowLayout {
            let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            layout.minimumLineSpacing = 10.0
            self.collectionView?.setCollectionViewLayout(layout, animated: true)
        }else{
            let ponto = gestureRecognizer.location(in: self.collectionView)
            let index = collectionView?.indexPathForItem(at: ponto)
            let layout = CATCarouselFlowLayout()
            layout.scrollDirection = .horizontal
            self.collectionView?.setCollectionViewLayout(layout, animated: true)
            self.setupLayout()
            self.collectionView?.scrollToItem(at: index!, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
}

extension ProdutoCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionViewLayout is CATCarouselFlowLayout {
            return CGSize(width: 250, height: 300)
        }
        let size = CGSize(width: UIScreen.main.bounds.size.width / 2 - 16, height: 225)
        return size
    }
}

extension ProdutoCollectionViewController: ModelDelegate {
    
    func modelUpdated() {
        self.collectionView?.reloadData()
        self.unlock()
        if (self.refreshControl != nil) {
            let formate = DateFormatter()
            formate.dateFormat = "MMM d, h:mm a"
            let title = "Last update: \(formate.string(from: Date()))"
            let dictionary = [NSForegroundColorAttributeName: UIColor.white]
            let attribute = NSAttributedString(string: title, attributes: dictionary)
            self.refreshControl?.attributedTitle = attribute
            self.refreshControl?.endRefreshing()
        }
    }
    
    func errorUpdating(error: NSError) {
        self.unlock()
        let alert = UIAlertController(title: "Products", message: "There was a problem loading products\nCheck your internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
