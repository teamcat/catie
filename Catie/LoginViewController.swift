//
//  LoginViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var viewBemVindo: UIView!
    @IBOutlet weak var imagemLogo: UIImageView!
    @IBOutlet weak var botaoLogin: UIButton!
    @IBOutlet weak var botaoSingUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.iniciarAnimacao()
    }
    
    @objc private func iniciarAnimacao() -> Void {
        
        let transicao = CGAffineTransform(translationX: 0, y: (self.view.center.y - self.imagemLogo.center.y) / 2)
        let scale = transicao.scaledBy(x: 3.0, y: 3.0)
        self.imagemLogo.transform = scale

        self.botaoLogin.alpha = 0
        self.botaoSingUp.alpha = 0
        
        let inicialBemVindo = CGAffineTransform(translationX: 0, y: -200)
        self.viewBemVindo.transform = inicialBemVindo
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveEaseIn], animations: {
            
            self.imagemLogo.transform = CGAffineTransform.identity
            
            }) { (f) in
                
                UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: [.curveLinear], animations: {
                    self.viewBemVindo.transform = CGAffineTransform.identity
                    }, completion: nil)
                
                UIView.animate(withDuration: 0.7, animations: {
                    self.botaoLogin.alpha = 1
                    self.botaoSingUp.alpha = 1
                })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func login() {
        let storyboard = UIStoryboard(name: "Loja", bundle: nil);
        let controller = storyboard.instantiateInitialViewController()! as UIViewController;
        
        self.present(controller, animated: true, completion: nil);
        
    }

}
