//
//  CATGradient.swift
//  Catie
//
//  Created by Thiago Vinhote on 20/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

@IBDesignable
class CATGradient: UIView {

    let gradientLayer: CAGradientLayer = CAGradientLayer()
    
    @IBInspectable
    var color1 : UIColor = UIColor.black{
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var color2: UIColor = UIColor.black {
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        gradientLayer.frame = self.bounds
        
        gradientLayer.colors = [color1.withAlphaComponent(0.0).cgColor, color2.withAlphaComponent(1.0).cgColor]
        
        gradientLayer.locations = [0.0, 1.0]
        
        self.layer.addSublayer(gradientLayer)
        
    }

}
