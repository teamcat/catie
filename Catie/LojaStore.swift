//
//  LojaStore.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit
import UIKit

class LojaStore: NSObject {
    
    let model : Model = Model.singleton;
    var lojas = [Loja]();
    var loja : Loja!;
    var delegate : ModelDelegate?
    
    class Loja {
        var record : CKRecord!;
        
        var nome : String {
            get{
                return self.record.object(forKey: "Nome") as! String;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Nome");
            }
        }
        
        var descricao: String{
            get{
                return self.record.object(forKey: "Descricao") as! String;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Descricao");
            }
        }
        
        var avaliacao : Int {
            get{
                return self.record.object(forKey: "Avaliacao") as! Int;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Avaliacao");
            }
        }
        
        var data: NSDate {
            get {
                return self.record.object(forKey: "Data") as! NSDate;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Data");
            }
        }
        
        var imagemCapa : CKAsset {
            get {
                return self.record.object(forKey: "ImagemCapa") as! CKAsset;
            }
            set (val){
                self.record.setValue(val as CKRecordValue?, forKey: "ImagemCapa");
            }
        }
        
        var imagemPerfil: CKAsset{
            get{
                return self.record.object(forKey: "ImagemPerfil") as! CKAsset;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "ImagemPerfil");
            }
        }
        
        var email: String {
            get {
                return self.record.object(forKey: "Email") as! String
            }
            set (val){
                self.record.setObject( val as CKRecordValue?, forKey: "Email")
            }
        }
        
        var senha: String{
            get{
                return self.record.object(forKey: "Senha") as! String
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Senha")
            }
        }
        
        var artesanal: Bool{
            get{
                return self.record.object(forKey: "Artesanal") as! Bool
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Artesanal")
            }
        }
        
        var combinar: Bool {
            get {
                return self.record.object(forKey: "Combinar") as! Bool
            }
            set (val) {
                self.record.setObject(val as CKRecordValue?, forKey: "Combinar")
            }
        }
        
        var cartao: Bool{
            get{
                return self.record.object(forKey: "Cartao") as! Bool
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Cartao")
            }
        }
        
        var encomenda: Bool{
            get{
                return self.record.object(forKey: "Encomenda") as! Bool
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Encomenda")
            }
        }
        
        var entrega: Bool{
            get{
                return self.record.object(forKey: "Entrega") as! Bool
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Entrega")
            }
        }
        
        var categoria: CategoriaStore.Categoria {
            get {
                return CategoriaStore.Categoria(record: self.record.object(forKey: "Categoria") as? CKRecord);
            }
            set (val){
                let reference = CKReference(recordID: val.record.recordID, action: .deleteSelf);
                self.record.setObject(reference as CKRecordValue?, forKey: "Categoria");
            }
        }
        
        var reference : CKReference{
            get {
                let reference = CKReference(recordID: self.record.recordID, action: .deleteSelf)
                return reference
            }
        }
        
        var produtos: [CKReference] {
            get {
                return self.record.object(forKey: "Produtos") as! [CKReference]
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Produtos")
            }
        }
        
        var referenceCategoria: CKReference {
            get {
                return self.record.object(forKey: "Categoria") as! CKReference
            }
        }
        
        var produtosRecordIDs: [CKRecordID] {
            get {
                let references = self.record.object(forKey: "Produtos") as! [CKReference];
                
                let recordIDs = references.map { (reference) -> CKRecordID in
                    return reference.recordID;
                }
                
                return recordIDs;
            }
        }
        
        
        
        init(dataBase: CKDatabase? = nil, record: CKRecord? = nil, recordName: String? = nil) {
            if let rc = recordName {
                self.record = (record != nil) ? record : CKRecord(recordType: "Loja", recordID: CKRecordID(recordName: rc))
            }else{
                self.record = (record != nil) ? record : CKRecord(recordType: "Loja");
            }
            
            if record == nil {
                self.nome = ""
                self.imagemCapa = uiImageToCKAsset(named: "quadros")
                self.imagemPerfil = uiImageToCKAsset(named: "canecas")
                self.artesanal = false
                self.cartao = false
                self.encomenda = false
                self.entrega = false
                self.combinar = false
                self.avaliacao = 0
            }
        }
        
        convenience init(nome: String, descricao: String, avaliacao: Int? = nil, data: NSDate, imagemCapa: CKAsset? = nil, imagemPerfil: CKAsset? = nil){
            self.init();
            
            self.nome = nome;
            self.descricao = descricao;
            self.avaliacao = avaliacao ?? 0
            self.data = data;
            self.imagemCapa = imagemCapa ?? CKAsset(fileURL: URL(fileURLWithPath: "canecas"))
            self.imagemPerfil = imagemPerfil ?? CKAsset(fileURL: URL(fileURLWithPath: "canecas"));
            self.artesanal = false
            self.cartao = false
            self.encomenda = false
            self.combinar = false
            self.entrega = false
        }
    }
    
    init(loja: Loja? = nil) {
        super.init();
        
        self.loja = loja ?? Loja();
        
    }
    
    func create(completion: @escaping (_ success: Bool, _ messagem: String, _ error: NSError?) -> Void){
        
        model.publicDB.save(loja.record) { (record, error) in
            DispatchQueue.main.async {
                if error != nil {
                    completion(false, "We found a problem at registering your account", error as NSError?);
                }else{
                    self.loja.record = record;
                    completion(true, "Account successfully created.", nil);
                }
            }
        }
    }
    
    func load(predicate: NSPredicate? = nil, sort: NSSortDescriptor? = nil){
        
//        self.lojas = self.lojasStore
//        self.delegate?.modelUpdated()
//        return
        
        let predicate = predicate ?? NSPredicate(value: true);
        let sort = sort ?? NSSortDescriptor(key: "Nome", ascending: false);
        let query = CKQuery(recordType: "Loja", predicate: predicate);
        query.sortDescriptors = [sort];
        
        model.publicDB.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error as! NSError);
                    print("Erro ao carregar: \((error as! NSError).description)");
                    return;
                }
            }else{
                self.lojas.removeAll();
                
                for record in results! {
                    let loja = Loja(record: record);
                    self.lojas.append(loja);
                }
                
                DispatchQueue.main.async {
                    self.delegate?.modelUpdated();
                    print("Sucesso ao carregar");
                    return;
                }
            }
        }
    }
    
    func search(recordID: CKRecordID, completion: @escaping (_ success: Bool, _ loja: Loja?, _ error: NSError?) -> Void){
        
        let fetch = CKFetchRecordsOperation(recordIDs: [recordID]);
        
        fetch.perRecordProgressBlock = {
            print($1);
        }
        
        fetch.perRecordCompletionBlock = { record, recordId, error in
            if error != nil {
                DispatchQueue.main.async {
                    print(error.debugDescription)
                    completion(false, nil, error as NSError?);
                }
            }else{
                DispatchQueue.main.async {
                    print(error.debugDescription)
                    completion(true, Loja(record: record), nil);
                }
            }
        }
        
        fetch.database = model.publicDB
        fetch.start()
    }
    
    func update(loja: Loja, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ) {
        self.loja = loja;
        
        create() { success, message, error in
            completion(success, message, error)
        }
    }
    
    func remove(recordId: CKRecordID, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ){
        model.publicDB.delete(withRecordID: recordId) { record, error in
            if error != nil {
                print("ERRO RECORD ID: \(self.loja.record.recordID)")
                completion(false, "CloudKit não pode remover", error as NSError?)
            } else {
                print("DEL RECORD ID: \(self.loja.record.recordID)")
                completion(true, "Deletado com sucesso", nil)
            }
        }
    }
    
    private var lojasStore : [Loja] = {
       
        var lojas = [Loja]()
        
        for i in 0...3 {
            let l = Loja()
            l.nome = "Loja \(i)"
            l.avaliacao = 1 * i + 1
            l.artesanal = (i % 2 == 0)
            l.entrega = (i % 2 != 0)
            l.cartao = (i % 2 == 0)
            l.encomenda = (i % 2 != 0)
            if (i % 2 == 0) {
                l.imagemCapa = uiImageToCKAsset(named: "quadros")
            }else{
                l.imagemCapa = uiImageToCKAsset(named: "quadros")
            }
            lojas.append(l)
        }
        
        let lojav = Loja()
        lojav.nome = "Loja verdade"
        lojav.avaliacao = 5
        lojav.artesanal = false
        lojav.entrega = false
        lojav.cartao = true
        lojav.encomenda = true
        lojav.imagemCapa = uiImageToCKAsset(named: "quadros")
        lojav.imagemPerfil = uiImageToCKAsset(named: "canecas")
        lojas.append(lojav)
        
        let lojaf = Loja()
        lojaf.nome = "Loja falso"
        lojaf.avaliacao = 5
        lojaf.artesanal = false
        lojaf.entrega = false
        lojaf.cartao = false
        lojaf.encomenda = false
        lojaf.imagemCapa = uiImageToCKAsset(named: "quadros")
        lojas.append(lojaf)
        return lojas
    }()
    
    
}
