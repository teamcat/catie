//
//  LojaTableViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 14/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CloudKit

class LojaTableViewController: UITableViewController {
    
    let lojaStore = LojaStore();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lock()
        lojaStore.delegate = self
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor.darkGray
        self.refreshControl?.tintColor = UIColor.white
        self.refreshControl?.addTarget(self, action: #selector(self.carregarDados), for: .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.lock()
        self.carregarDados()
    }
    
    @objc private func carregarDados(){
        lojaStore.load()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows

        return self.lojaStore.lojas.count;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseID", for: indexPath) as! LojaTableViewCell
        
        // Configure the cell...
        cell.loja = self.lojaStore.lojas[indexPath.row]
        
        return cell;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewLoja" {
            let destino = segue.destination as! LojaDetalheViewController
            destino.loja = self.lojaStore.lojas[self.tableView.indexPathForSelectedRow!.row]
        }
    }
}

extension LojaTableViewController : ModelDelegate{
    
    func modelUpdated() {
        self.tableView.reloadData()
        self.unlock()
        if (self.refreshControl != nil) {
            let formate = DateFormatter()
            formate.dateFormat = "MMM d, h:mm a"
            let title = "Last update: \(formate.string(from: Date()))"
            let dictionary = [NSForegroundColorAttributeName: UIColor.white]
            let attribute = NSAttributedString(string: title, attributes: dictionary)
            self.refreshControl?.attributedTitle = attribute
            self.refreshControl?.endRefreshing()
        }
    }
    
    func errorUpdating(error: NSError) {
        if self.refreshControl != nil {
            self.refreshControl?.endRefreshing()
        }
        self.unlock()
        let alert = UIAlertController(title: "Stores", message: "There was a problem loading stores\nCheck your internet connection.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
