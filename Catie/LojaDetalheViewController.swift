//
//  LojaDetalheViewController.swift
//  Catie
//
//  Created by Thiago Vinhote on 24/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit
import CloudKit

class LojaDetalheViewController: UITableViewController {
    
    var loja : LojaStore.Loja!
    var produtoStore = ProdutoStore()
    var lojaStore : LojaStore!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.backgroundColor = UIColor.darkGray
        self.refreshControl?.tintColor = UIColor.white
        self.refreshControl?.addTarget(self, action: #selector(self.carregarDados), for: .valueChanged)
        
        produtoStore.delegate = self
        lojaStore = LojaStore(loja: self.loja)
        lojaStore.search(recordID: self.loja.record.recordID) { (sucesso, loja, error) in
            if sucesso {
                self.loja = loja
                print(self.loja.record.recordID.recordName)
                self.title = self.loja.nome
                self.carregarDados()
            } else {
                print(error?.description)
            }
        }
    }
    
    @objc private func carregarDados(){
        produtoStore.load(predicate: NSPredicate(format: "Loja = %@", loja.reference))
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.produtoStore.produtos.count) + 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLoja") as! LojaDetalheTableViewCell
            
            cell.loja = loja
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLojaProduto") as! LojaProdutoTableViewCell
            
            cell.produtos = (produtoStore.produtos[indexPath.row - 1], produtoStore.produtos[indexPath.row - 1])
            
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 317
        }else{
            return 240
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LojaDetalheViewController: ModelDelegate {
    
    func modelUpdated() {
        self.tableView.reloadData()
        if (self.refreshControl != nil) {
            let formate = DateFormatter()
            formate.dateFormat = "MMM d, h:mm a"
            let title = "Last update: \(formate.string(from: Date()))"
            let dictionary = [NSForegroundColorAttributeName: UIColor.white]
            let attribute = NSAttributedString(string: title, attributes: dictionary)
            self.refreshControl?.attributedTitle = attribute
            self.refreshControl?.endRefreshing()
        }
    }
    
    func errorUpdating(error: NSError) {
        
    }
    
}
