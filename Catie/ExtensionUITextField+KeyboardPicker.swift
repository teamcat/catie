//
//  ExtensionUITextField+KeyboardPicker.swift
//  Catie
//
//  Created by Thiago Vinhote on 18/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit


extension UITextField {
    
    func keyboardPicker(_ options: Array<String>, controller: UIViewController){
        let picker = UIPickerView()
        picker.delegate = controller as? UIPickerViewDelegate
        picker.dataSource = controller as? UIPickerViewDataSource
        self.inputView = picker
        
//        picker.backgroundColor = UIColor.colorFromHex(0x2A2A2A)
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: controller.view.frame.size.height/6, width: controller.view.frame.size.width, height: 40.0))
//        toolBar.backgroundColor = UIColor.red
        toolBar.layer.position = CGPoint(x: controller.view.frame.size.width/2, y: controller.view.frame.size.height-20.0)
//        toolBar.barStyle = UIBarStyle.blackTranslucent
//        toolBar.tintColor = UIColor.colorFromHex(0xCCCCCC)
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        
        toolBar.setItems([flexSpace, doneButton], animated: true)
        self.inputAccessoryView = toolBar
        
//        self.text = options[0]
    }
    
    func donePressed(_ sender: UIBarButtonItem) {
        self.resignFirstResponder()
    }
}
