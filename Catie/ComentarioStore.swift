//
//  ComentarioStore.swift
//  Catie
//
//  Created by Thiago Vinhote on 15/09/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import Foundation
import CloudKit

class ComentarioStore: NSObject {

    let model : Model = Model.singleton;
    var comentarios = [Comentario]();
    var comentario : Comentario!
    var delegate : ModelDelegate?
    
    class Comentario {
        
        var record : CKRecord!
        
        var comentario: String{
            get {
                return self.record.object(forKey: "Comentario") as! String;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Comentario");
            }
        }
        
        var data: NSDate{
            get {
                return self.record.object(forKey: "Data") as! NSDate;
            }
            set (val){
                self.record.setObject(val as CKRecordValue?, forKey: "Data");
            }
        }
        
        init(dataBase: CKDatabase? = nil, record: CKRecord? = nil) {
            self.record = (record != nil) ? record : CKRecord(recordType: "Comentario");
        }
        
        convenience init(comentario: String, data: NSDate){
            self.init();
            
            self.comentario = comentario;
            self.data = data;
        }
    }
    
    init(comentario: Comentario? = nil) {
        super.init();
        
        self.comentario = comentario ?? Comentario();
    }
    
    func create(completion: @escaping (_ success: Bool, _ messagem: String, _ error: NSError?) -> Void){
        
        model.publicDB.save(comentario.record) { (record, error) in
            DispatchQueue.main.async {
                if error != nil {
                    completion(false, "Não foi possivel salvar no CloudKit", error as NSError?);
                }else{
                    self.comentario.record = record;
                    completion(true, "Salvo com sucesso", nil);
                }
            }
        }
    }
    
    func load(predicate: NSPredicate? = nil, sort: NSSortDescriptor? = nil){
        let predicate = predicate ?? NSPredicate(value: true);
        let sort = sort ?? NSSortDescriptor(key: "Data", ascending: false);
        let query = CKQuery(recordType: "Comentario", predicate: predicate);
        query.sortDescriptors = [sort];
        
        model.publicDB.perform(query, inZoneWith: nil) { (results, error) in
            if error != nil {
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error as! NSError);
                    print("Erro ao carregar: \((error as! NSError).description)");
                    return;
                }
            }else{
                self.comentarios.removeAll();
                
                for record in results! {
                    let comentario = Comentario(record: record);
                    self.comentarios.append(comentario);
                }
                
                DispatchQueue.main.async {
                    self.delegate?.modelUpdated();
                    print("Sucesso ao carregar.");
                    return;
                }
            }
        }
    }
    
    func update(comentario: Comentario, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ) {
        self.comentario = comentario;
        
        create() { success, message, error in
            completion(success, message, error)
        }
    }
    
    func remove(recordId: CKRecordID, completion: @escaping (_ success: Bool, _ message: String, _ error: NSError?) -> () ){
        model.publicDB.delete(withRecordID: recordId) { record, error in
            if error != nil {
                print("ERRO RECORD ID: \(self.comentario.record.recordID)")
                completion(false, "CloudKit não pode remover", error as NSError?)
            } else {
                print("DEL RECORD ID: \(self.comentario.record.recordID)")
                completion(true, "Deletado com sucesso", nil)
            }
        }
    }

   
}
