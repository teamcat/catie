//
//  CadastroViewController.swift
//  Catie
//
//  Created by Arthur Melo on 9/19/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LojaCadastroViewController: UIViewController {

    let loja = LojaStore.Loja()
    
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var imagemLoja: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var imagemPerfil: UIImageView!
    @IBOutlet weak var confirmarSenhaTextField: UITextField!
    @IBOutlet weak var categoriaTextField: UITextField!
    @IBOutlet weak var navItem: UINavigationItem!

    internal var categorias: [CategoriaCD]!
    var imagem: UIImageView!
    weak var categoriaSelecionada: CategoriaCD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.colorFromHex(0xFFCD01)
        imagemPerfil.layer.borderWidth = 2
        imagemPerfil.layer.borderColor = UIColor.white.cgColor
        
        CategoriaStoreCD.singleton.get(){ sucesso, categorias, error in
            if sucesso {
                self.categorias = categorias!
                
                self.categoriaTextField.keyboardPicker(categorias!.map({ (categoria) -> String in
                    return categoria.nome!
                }) , controller: self)
            } else {
                print(error?.description)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func voltar(_ sender: UIBarButtonItem){
        print(#function)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc internal func tapView(){
        self.view.endEditing(true)
    }
    
    @IBAction func savePressed(_ sender: UIBarButtonItem) {
        //lojaStore.loja = LojaStore.Loja(nome: nomeTextField.text!, descricao: nil, avaliacao: nil, data: nil, imagemCapa: nil, imagemPerfil: nil)
        
        if nomeTextField.text != "" && emailTextField.text != "" && senhaTextField.text != "" && confirmarSenhaTextField.text != "" {
            if senhaTextField.text == confirmarSenhaTextField.text {
                performSegue(withIdentifier: "nextPage", sender: self)
            } else {
                let alertControl = UIAlertController(title: "Passwords do not match", message: "Check your typing and try again", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                
                alertControl.addAction(action)
                self.present(alertControl, animated: true, completion: nil)
            }
        } else {
            let alertControl = UIAlertController(title: "Missing information", message: "We need all those fields!", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            
            alertControl.addAction(action)
            self.present(alertControl, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func addCoverImage(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            imagem = self.imagemLoja
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func tapToProfilePicture(_ sender: UITapGestureRecognizer) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            
            imagem = self.imagemPerfil

            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nextPage" {
            loja.nome = nomeTextField.text!
            loja.email = emailTextField.text!
            loja.senha = senhaTextField.text!
            if imagemLoja.image != nil {
                loja.imagemCapa = uiImageToCKAsset(imagem: imagemLoja.image!)
            } else {
                loja.imagemCapa = uiImageToCKAsset(imagem: #imageLiteral(resourceName: "camera"))
            }
            if imagemPerfil.image != nil {
                loja.imagemPerfil = uiImageToCKAsset(imagem: imagemPerfil.image!)
            } else {
                loja.imagemPerfil = uiImageToCKAsset(imagem: #imageLiteral(resourceName: "camera"))
            }
            loja.categoria = CategoriaStore.Categoria(recordName: self.categoriaSelecionada.recordID!)
            
            let destinationVC = segue.destination as! LojaDadosViewController
            destinationVC.loja = loja
        }
    }
}

extension LojaCadastroViewController : UIImagePickerControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil);
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagem.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        imagem.contentMode = .scaleAspectFill
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension LojaCadastroViewController : UINavigationControllerDelegate{
    
}

extension LojaCadastroViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.categoriaSelecionada = self.categorias[row]
        self.categoriaTextField.text = self.categoriaSelecionada.nome!
    }
}

extension LojaCadastroViewController : UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categorias.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categorias[row].nome!
    }
}

extension LojaCadastroViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let distanciaVertical = self.view.frame.height - textField.frame.origin.y - textField.frame.size.height / 2
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform(translationX: 0, y: (distanciaVertical - 280) < 0 ? (distanciaVertical - 280
                ) : 0)
            
            }, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            
            self.view.transform = CGAffineTransform.identity
            
            }, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tapView()
        return true
    }
    
}

