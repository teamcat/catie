//
//  DadosLojaViewController.swift
//  Catie
//
//  Created by Arthur Melo on 9/19/16.
//  Copyright © 2016 Team CAT. All rights reserved.
//

import UIKit

class LojaDadosViewController: UIViewController {

    @IBOutlet weak var entregaButton: UIButton!
    @IBOutlet weak var cartaoButton: UIButton!
    @IBOutlet weak var artesanalButton: UIButton!
    @IBOutlet weak var encomendasButton: UIButton!
    @IBOutlet weak var combinarButton: UIButton!
    @IBOutlet weak var encomendas: UILabel!
    @IBOutlet weak var encomendasDesc: UILabel!
    @IBOutlet weak var entrega: UILabel!
    @IBOutlet weak var entregaDesc: UILabel!
    @IBOutlet weak var combinar: UILabel!
    @IBOutlet weak var combinarDesc: UILabel!
    @IBOutlet weak var artesanal: UILabel!
    @IBOutlet weak var artesanalDesc: UILabel!
    @IBOutlet weak var cartao: UILabel!
    @IBOutlet weak var cartaoDesc: UILabel!
    
    var lojaStore : LojaStore!
    weak var loja: LojaStore.Loja!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lojaStore = LojaStore(loja: self.loja)
        print(loja.email)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func botaoPressionado(_ sender: UIButton) {
        switch sender {
        case entregaButton:
            if lojaStore.loja.entrega == true {
                lojaStore.loja.entrega = false
                entregaButton.setImage(#imageLiteral(resourceName: "entregas"), for: .normal)
                entrega.textColor = UIColor.colorFromHex(0xCDCDCD)
                entregaDesc.textColor = UIColor.colorFromHex(0xCDCDCD)
            } else {
                print("Entrega disponível")
                lojaStore.loja.entrega = true
                entregaButton.setImage(#imageLiteral(resourceName: "entregaSelecionado"), for: .normal)
                entrega.textColor = UIColor.colorFromHex(0xFFCD01)
                entregaDesc.textColor = UIColor.colorFromHex(0xFFCD01)
            }
            
        case cartaoButton:
            if lojaStore.loja.cartao == true {
                lojaStore.loja.cartao = false
                cartaoButton.setImage(#imageLiteral(resourceName: "cartao"), for: .normal)
                cartao.textColor = UIColor.colorFromHex(0xCDCDCD)
                cartaoDesc.textColor = UIColor.colorFromHex(0xCDCDCD)
            } else {
                print("Aceita cartão")
                cartaoButton.setImage(#imageLiteral(resourceName: "cartaoSelecionado"), for: .normal)
                cartao.textColor = UIColor.colorFromHex(0xFFCD01)
                cartaoDesc.textColor = UIColor.colorFromHex(0xFFCD01)
                lojaStore.loja.cartao = true
            }
        case artesanalButton:
            if lojaStore.loja.artesanal == true {
                lojaStore.loja.artesanal = false
                artesanalButton.setImage(#imageLiteral(resourceName: "artesanal"), for: .normal)
                artesanal.textColor = UIColor.colorFromHex(0xCDCDCD)
                artesanalDesc.textColor = UIColor.colorFromHex(0xCDCDCD)
            } else {
                print("Produtos artesanais")
                artesanalButton.setImage(#imageLiteral(resourceName: "artesanalSelecionado"), for: .normal)
                artesanal.textColor = UIColor.colorFromHex(0xFFCD01)
                artesanalDesc.textColor = UIColor.colorFromHex(0xFFCD01)
                lojaStore.loja.artesanal = true
            }
        case encomendasButton:
            if lojaStore.loja.encomenda == true {
                lojaStore.loja.encomenda = false
                encomendasButton.setImage(#imageLiteral(resourceName: "encomendas"), for: .normal)
                encomendas.textColor = UIColor.colorFromHex(0xCDCDCD)
                encomendasDesc.textColor = UIColor.colorFromHex(0xCDCDCD)
            } else {
                print("Faz encomendas")
                encomendasButton.setImage(#imageLiteral(resourceName: "encomendasSelecionado"), for: .normal)
                encomendas.textColor = UIColor.colorFromHex(0xFFCD01)
                encomendasDesc.textColor = UIColor.colorFromHex(0xFFCD01)
                lojaStore.loja.encomenda = true
            }
        // Adicionar consulta com o servidor
        case combinarButton:
            if lojaStore.loja.combinar == true {
                lojaStore.loja.combinar = false
                combinarButton.setImage(#imageLiteral(resourceName: "combinar"), for: .normal)
                combinar.textColor = UIColor.colorFromHex(0xCDCDCD)
                combinarDesc.textColor = UIColor.colorFromHex(0xCDCDCD)
            } else {
                print("A combinar")
                combinarButton.setImage(#imageLiteral(resourceName: "combinarSelecionado"), for: .normal)
                combinar.textColor = UIColor.colorFromHex(0xFFCD01)
                combinarDesc.textColor = UIColor.colorFromHex(0xFFCD01)
                lojaStore.loja.combinar = true
            }
        default:
            print("Nenhum botão")
        }
    }
    
    @IBAction func criarLoja(_ sender: UIBarButtonItem) {
        self.lock()
        sender.isEnabled = false
        lojaStore.create { (success, mensagem, error) in
            var alertControl: UIAlertController!
            if success {
                print(mensagem)
                self.salvarLojaCoreData()
                
                alertControl = UIAlertController(title: "Congratulations", message: mensagem, preferredStyle: .alert)
                let action = UIAlertAction(title: "Start", style: .default, handler: { (action) in
                    if success {
                        let storyboard = UIStoryboard(name: "Loja", bundle: nil);
                        let controller = storyboard.instantiateInitialViewController()! as UIViewController;
                        
                        self.present(controller, animated: true, completion: nil);
                    }
                })
                alertControl.addAction(action)
            } else {
                print(mensagem, error?.description)
                
                alertControl = UIAlertController(title: "Try again", message: "\(mensagem)\nCheck your internet connection.", preferredStyle: .alert)
                let action = UIAlertAction(title: "Close", style: .default, handler: { (action) in
                    sender.isEnabled = true
                })
                alertControl.addAction(action)
            }
            self.unlock()
            self.present(alertControl, animated: true,completion: nil);
        }
    }
    
    private func salvarLojaCoreData() -> Void{
        let l = LojaStoreCD.singleton.create()
        l.nome = lojaStore.loja.nome
        l.email = lojaStore.loja.email
        l.imagemCapa = ckAssetToUIImage(asset: lojaStore.loja.imagemCapa)
        l.imagemPerfil = ckAssetToUIImage(asset: lojaStore.loja.imagemPerfil)
        l.senha = lojaStore.loja.senha
        l.artesanal = lojaStore.loja.artesanal as NSNumber?
        l.cartao = lojaStore.loja.cartao as NSNumber?
        l.encomenda = lojaStore.loja.encomenda as NSNumber?
        l.entrega = lojaStore.loja.entrega as NSNumber?
        l.recordID = lojaStore.loja.record.recordID.recordName
        l.recordReferenceCategoria = lojaStore.loja.referenceCategoria.recordID.recordName
        l.combinar = lojaStore.loja.combinar as NSNumber?
        LojaStoreCD.singleton.save(loja: l)
    }
}


